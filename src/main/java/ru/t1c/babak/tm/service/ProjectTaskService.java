package ru.t1c.babak.tm.service;

import ru.t1c.babak.tm.api.repository.IProjectRepository;
import ru.t1c.babak.tm.api.repository.ITaskRepository;
import ru.t1c.babak.tm.api.service.IProjectTaskService;
import ru.t1c.babak.tm.exception.entity.ProjectNotFoundException;
import ru.t1c.babak.tm.exception.entity.TaskNotFoundException;
import ru.t1c.babak.tm.exception.field.IdEmptyException;
import ru.t1c.babak.tm.exception.field.UserIdEmptyException;
import ru.t1c.babak.tm.model.Project;
import ru.t1c.babak.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String userId, final String projectId, final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()
                || taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(final String userId, final String projectId, final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()
                || taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new IdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        if (!task.getProjectId().equals(projectId)) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

    @Override
    public void removeProjectById(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        for (Task task : tasks) taskRepository.removeById(userId, task.getId());
        projectRepository.removeById(projectId);
    }

}
