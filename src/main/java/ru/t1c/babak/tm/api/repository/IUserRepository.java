package ru.t1c.babak.tm.api.repository;

import ru.t1c.babak.tm.enumerated.Role;
import ru.t1c.babak.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    User create(String login);

    User create(String login, String passwordHash);

    User create(String login, String passwordHash, String email);

    User create(String login, String passwordHash, Role role);

    User removeByLogin(String login);

    User setPasswordHash(User user, String passwordHash);

    User setPasswordHashById(String id, String passwordHash);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}
