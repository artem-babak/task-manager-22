package ru.t1c.babak.tm.api.service;

import ru.t1c.babak.tm.enumerated.Role;
import ru.t1c.babak.tm.model.Task;
import ru.t1c.babak.tm.model.User;

import java.util.List;

public interface IUserService extends IService<User>{

    User findByLogin(String login);


    User findByEmail(String email);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String id, String password);

    User updateById(String id, String firstName, String lastName, String middleName);

    User updateByLogin(String login, String firstName, String lastName, String middleName);


    User removeByLogin(String login);

    User removeByEmail(String email);

    void lockUserByLogin(String login);

    void unlockUserByLogin(String login);

}
