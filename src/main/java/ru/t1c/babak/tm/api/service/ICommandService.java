package ru.t1c.babak.tm.api.service;

import ru.t1c.babak.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    Collection<AbstractCommand> getTerminalCommands();

    void add(AbstractCommand command);

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArgument(String argument);

}
