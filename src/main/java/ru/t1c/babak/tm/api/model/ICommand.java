package ru.t1c.babak.tm.api.model;

public interface ICommand {

    String getName();

    String getDescription();

    String getArgument();

}
