package ru.t1c.babak.tm.api.service;

import ru.t1c.babak.tm.api.repository.IUserOwnedRepository;
import ru.t1c.babak.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

}
