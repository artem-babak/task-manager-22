package ru.t1c.babak.tm.api.repository;

import ru.t1c.babak.tm.enumerated.Sort;
import ru.t1c.babak.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    int getSize(String userId);

    M add(String userId, M model);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator comparator);

    List<M> findAll(String userId, Sort sort);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    boolean existsById(String userId, String id);

    M remove(String userId, M model);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

    void clear(String userId);

}
