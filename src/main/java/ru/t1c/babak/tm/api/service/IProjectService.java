package ru.t1c.babak.tm.api.service;

import ru.t1c.babak.tm.enumerated.Status;
import ru.t1c.babak.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IUserOwnedService<Project> {

    Project updateById(String userId, String id, String name, String description);

    Project updateByIndex(String userId, Integer index, String name, String description);

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

    Project create(String userId, String name, String description, Date dateBegin, Date dateEnd);

    Project changeProjectStatusById(String userId, String id, Status status);

    Project changeProjectStatusByIndex(String userId, Integer index, Status status);


}
