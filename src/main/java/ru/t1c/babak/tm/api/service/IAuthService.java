package ru.t1c.babak.tm.api.service;

import ru.t1c.babak.tm.enumerated.Role;
import ru.t1c.babak.tm.model.User;

public interface IAuthService {

    void registry(String login, String password, String email);

    void login(String login, String password);

    void logout();

    boolean isAuth();

    String getUserId();

    User getUser();

    void checkRoles(Role[] roles);

}
