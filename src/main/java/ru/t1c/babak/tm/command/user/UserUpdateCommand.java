package ru.t1c.babak.tm.command.user;

import ru.t1c.babak.tm.command.AbstractCommand;
import ru.t1c.babak.tm.enumerated.Role;
import ru.t1c.babak.tm.util.TerminalUtil;

public final class UserUpdateCommand extends AbstractCommand {

    public static final String NAME = "user-update";

    public static final String DESCRIPTION = "Update current user.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER]");
        final String userId = getServiceLocator().getAuthService().getUserId();
        System.out.println("\tENTER FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("\tENTER LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("\tENTER MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        getServiceLocator().getUserService().updateById(userId, firstName, lastName, middleName);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
