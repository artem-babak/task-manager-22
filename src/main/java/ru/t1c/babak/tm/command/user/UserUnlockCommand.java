package ru.t1c.babak.tm.command.user;

import ru.t1c.babak.tm.enumerated.Role;
import ru.t1c.babak.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    public static final String NAME = "user-unlock";

    public static final String DESCRIPTION = "Unlock user.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().unlockUserByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
