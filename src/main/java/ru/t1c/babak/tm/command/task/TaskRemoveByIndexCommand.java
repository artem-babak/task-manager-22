package ru.t1c.babak.tm.command.task;

import ru.t1c.babak.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    public static final String NAME = "task-remove-by-index";

    public static final String DESCRIPTION = "Remove task by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        renderAllTasks();
        System.out.println("\tENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        getTaskService().removeByIndex(userId, index);
    }

}
