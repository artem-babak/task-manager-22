package ru.t1c.babak.tm.command.task;

import ru.t1c.babak.tm.enumerated.Status;
import ru.t1c.babak.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-start-by-id";

    public static final String DESCRIPTION = "Start task by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("\tENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
    }

}
