package ru.t1c.babak.tm.command.user;

import ru.t1c.babak.tm.command.AbstractCommand;
import ru.t1c.babak.tm.enumerated.Role;
import ru.t1c.babak.tm.exception.entity.UserNotFoundException;
import ru.t1c.babak.tm.model.User;

public final class UserViewProfileCommand extends AbstractCommand {

    public static final String NAME = "user-view-profile";

    public static final String DESCRIPTION = "View current user profile.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[VIEW USER PROFILE]");
        final User user = getServiceLocator().getAuthService().getUser();
        if (user == null) throw new UserNotFoundException();
        System.out.println("\tID: " + user.getId());
        System.out.println("\tLOGIN: " + user.getLogin());
        System.out.println("\tFIRST NAME: " + user.getFirstName());
        System.out.println("\tLAST NAME: " + user.getLastName());
        System.out.println("\tMIDDLE NAME: " + user.getMiddleName());
        System.out.println("\tE-MAIL: " + user.getEmail());
        System.out.println("\tROLE: " + user.getRole().getDisplayName());
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
