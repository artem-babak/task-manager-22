package ru.t1c.babak.tm.command.project;

import ru.t1c.babak.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-remove-by-id";

    public static final String DESCRIPTION = "Remove project by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("\tENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().findOneById(userId, id);
        getServiceLocator().getProjectTaskService().removeProjectById(userId, id);
    }

}
